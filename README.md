# st - simple terminal

This is my patched version of st made for my tastes. The only change I have made in config.h is to adjust the font size, the default is in config.def.h .

## Patches
I have applyed the following patches

1)[Most of the scroll back patches](https://st.suckless.org/patches/scrollback/)
2)[The delete key patch](https://st.suckless.org/patches/delkey/st-delkey-20201112-4ef0cbd.diff)
3)[The any size patch](https://st.suckless.org/patches/anysize/st-anysize-20220718-baa9357.diff)

## Requirements
In order to build st you need the Xlib header files.


## Installation
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install
    or
    sudo make clean install


## Running st
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.


## Credits
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

